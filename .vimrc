colo peachpuff
inoremap jk <Esc>
inoremap kj <Esc>

nnoremap gh ^
vnoremap gh ^
nnoremap gl $
vnoremap gl $
nnoremap gk gg
vnoremap gk gg
nnoremap gj G
vnoremap gj G

nnoremap tn :NERDTreeToggle<cr>

set history=700

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ";"
let g:mapleader = ";"

" Set 7 lines to the cursor - when moving vertically using j/k
set so=3

" Ignore compiled files
set wildignore=*.o,*~,*.pyc,*/node_modules/*,*/venv/*

"Always show current position
set ruler

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase
" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" Enable syntax highlighting
syntax enable

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac


" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile


" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
"set wrap "Wrap lines


" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>


" Treat long lines as break lines (useful when moving around in them)
"map j gj
"map k gk

" Disable highlight when <leader><cr> is pressed
"map <silent> <leader><cr> :noh<cr>
map <silent> <leader><space> :noh<cr>

map <space> /


" Smart way to move between windows
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()

" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Toggle paste mode on and off
"map <leader><leader>p :setlocal paste!<cr>
map <leader>p :setlocal paste!<cr>

"map <leader><leader>n :setlocal number!<cr>
map <leader>n :setlocal number!<cr>

"set cursorline
"hi CursorLine term=bold cterm=bold guibg=Grey40

let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"



"air-line
set laststatus=2
set t_Co=256
let g:airline_theme='powerlineish'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
"let g:airline#extensions#branch#displayed_head_limit = 16
let g:airline#extensions#branch#enabled = 0
let g:airline_skip_empty_sections = 1
let g:airline_section_b=''

if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline_powerline_fonts = 1

set hidden

" Move to the next buffer
"nmap <leader><Tab> :bnext<CR>
nmap <Tab> :bnext<CR>

" Move to the previous buffer
"nmap <leader><S-Tab> :bprevious<CR>
nmap <S-Tab> :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
"nmap <leader><leader>q :bp <BAR> bd #<CR>
nmap <C-c> :bp <BAR> bd #<CR>

execute pathogen#infect()
filetype plugin indent on

" easymotion

let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
"nmap s <Plug>(easymotion-overwin-f2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)


highlight ColorColumn ctermbg=6
set completeopt-=preview

set splitright
set splitbelow

hi clear VertSplit
hi clear Folded
let g:airline_theme='dark'

hi VertSplit term=bold cterm=bold ctermfg=234 ctermbg=234 gui=bold guifg=White guibg=Gray45
"hi Folded term=standout ctermfg=4 guifg=Blackk guibg=#e3c1a5
hi Folded term=standout ctermfg=240 guifg=Black guibg=#e3c1a5
hi Search cterm=NONE ctermfg=grey ctermbg=blue

if &diff
    colorscheme default
endif
