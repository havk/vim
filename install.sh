pushd bundle

git clone https://github.com/ctrlpvim/ctrlp.vim
git clone https://github.com/scrooloose/nerdtree
git clone https://github.com/bling/vim-airline
git clone https://github.com/vim-airline/vim-airline-themes
git clone https://github.com/easymotion/vim-easymotion
git clone https://github.com/nathanaelkane/vim-indent-guides
git clone https://github.com/bkad/camelcasemotion

popd
cp dark.vim bundle/vim-airline/autoload/airline/themes/
